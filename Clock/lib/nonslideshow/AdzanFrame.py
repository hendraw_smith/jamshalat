from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtCore import Qt
from PySide2.QtGui import QColor

class AdzanFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):

        QtWidgets.QFrame.__init__(self, parent)
        self.setObjectName("adzanFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#adzanFrame { background-color: blue; border-image: url(" +
            config.adzanbg+") 0 0 0 0 stretch stretch;}")
        self.setVisible(False)
        self.config = config
        self.xscale = float(width) / 1440.0
        self.yscale = float(height) / 900.0

        print("make infoAdzan")
        self.infoAdzan = QtWidgets.QLabel(self)
        self.infoAdzan.setObjectName("infoAdzan")
        infoAdzanRect = QtCore.QRect(
            width / 2 - height * .4,
            height * .45 - height * .4,
            height * .8,
            height * .8)
        self.infoAdzan.setGeometry(infoAdzanRect)
        dcolor = QColor(config.digitalcolor3).darker(0).name()
        lcolor = QColor(config.digitalcolor3).lighter(120).name()
        self.infoAdzan.setStyleSheet(
            "#infoAdzan { background-color: red; font-family:sans-serif;" +
            " font-weight: light; color: " +
            lcolor +
            "; background-color: transparent; font-size: " +
            str(int(config.digitalsize4 * self.xscale)) +
            "px;}")
        self.infoAdzan.setAlignment(Qt.AlignCenter)
        self.infoAdzan.setGeometry(infoAdzanRect)
        glow = QtWidgets.QGraphicsDropShadowEffect()
        glow.setOffset(0)
        glow.setBlurRadius(50)
        glow.setColor(QColor(dcolor))
        self.infoAdzan.setGraphicsEffect(glow)
        print("made infoAdzan")

        self.soundless = QtWidgets.QLabel(self)
        self.soundless.setObjectName("soundless")
        self.soundless.setStyleSheet("#soundless {background-color: transparent;border-radius: 25px;}")
        pixmap = QtGui.QPixmap(self.config.soundless)
        height_label = 150
        width_label = 150
        self.soundless.resize(width_label, height_label)
        self.soundless.setPixmap(pixmap.scaled(self.soundless.size(), QtCore.Qt.IgnoreAspectRatio))
        self.soundless.setGeometry(100 * self.xscale, 150 * self.yscale, 1000 * self.xscale, 500 * self.yscale)


    def showAdzanInfo(self):
        # Display the amount of time until the alarm goes off
        #print("Alarm set to go off in %s" % deltastr_adzan)
        self.infoAdzan.setGeometry(260 * self.xscale, 380 * self.yscale, 2000 * self.xscale, 500 * self.yscale)
        self.infoAdzan.setStyleSheet("#infoAdzan { font-family:comic-sans; font-weight:bold ;color: " +
             self.config.textcolor2 +
             "; background-color: transparent; font-size: " +
             str(int(30 * self.xscale)) + "px;}")
        self.infoAdzan.setAlignment(Qt.AlignLeft)
        self.infoAdzan.setText(self.config.TextAdzan)

