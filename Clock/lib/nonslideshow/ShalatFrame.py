from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtCore import Qt

from lib.AnalogClock import AnalogClock

class ShalatFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):

        QtWidgets.QFrame.__init__(self, parent)

        self.setObjectName("shalatFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#shalatFrame { background-color: blue; border-image: url(" +
            config.shalatBackground+") 0 0 0 0 stretch stretch;}")
        self.setVisible(False)
        self.config=config
        self.lastmin9 = -1
        # Jam analog di frame 7 (frame saat shalat)

        self.theClock = AnalogClock.makeClock("time",self,
            width / 2.75 - height * .15,
            height * .1,
            height * .8,
            height * .8,
            config.clockface,
            config.hourhand,
            config.minhand,
            config.sechand
            )

    def updateClock(self, now):
        self.theClock.draw(now)
