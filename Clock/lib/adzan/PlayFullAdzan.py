
class PlayFullAdzan:
    def play(self, kind):
        if self.mute:
            return
        isFajr = (kind=="SHUBUH")
        print("kind = %s"%kind)
        self.playadzan(isFajr)
    def playadzan(self, isFajr):
        mixer.init()
        if isFajr:
            print("playing adzan shubuh")
            mixer.music.load(self.config.adzan_fajr)
        else:
            print("playing common adzan")
            mixer.music.load(self.config.adzans)
        mixer.music.set_volume(self.volume/100.0)
        mixer.music.play()
    def __init__(self, config):
        self.config = config
        self.mute = False
        self.volume = 100
    def setVolume(self, volume):
        self.volume = volume
    def mute(self):
        self.mute = True
    def unmute(self):
        self.mute = False
