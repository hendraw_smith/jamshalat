
from pygame import mixer
class NotifyAdzan:
    def play(self, kind):
        if self.mute:
            return
        mixer.init()
        adzanFilenames={
        "SHUBUH": self.config.notify_fajr,
        "ZHUHUR": self.config.notify_zhuhur,
        "ASHAR": self.config.notify_ashar,
        "MAGHRIB": self.config.notify_maghrib,
        "ISYA": self.config.notify_isya,
        }
        mixer.music.load(adzanFilenames[kind])
        mixer.music.set_volume(self.volume/100.0)
        mixer.music.play()
    def __init__(self, config):
        self.config = config
        self.mute = False
        self.volume = 100
    def setVolume(self, volume):
        self.volume = volume
    def mute(self):
        self.mute = True
    def unmute(self):
        self.mute = False
