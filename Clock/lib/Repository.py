from lib.model.WaktuShalat import WaktuShalat
from lib.model.WaktuShalat import JadwalSholat
class Repository:

    def getJson(self, url):
        import requests,json
        r = requests.get(url)
        # return r.json()
        return json.loads(r.text)

    def getHadits(self):
        try:
            return self.getJson(f"{self.SERVER}/slide/")
        except:
            return {}

    def getSetting(self):
        return self.getJson(f"{self.SERVER}/setting/")

    def getInfo(self):
        return self.getJson(f"{self.SERVER}/pengumuman/")

    def getJadwalSholat(self):
        import json,ast
        dataStr = '''
[
    {"nama":"imsak","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"fajr","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"sunrise","waktu":"05:51","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"dhuhr","waktu":"12:00","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"asr","waktu":"15:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"sunset","waktu":"17:52","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"maghrib","waktu":"17:57","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"isha","waktu":"19:08","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
    {"nama":"midnight","waktu":"23:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
]
'''
        # data = json.loads(dataStr)
        data = self.getJson(f"{self.SERVER}/sholat/api/")
        times = ast.literal_eval(json.dumps(data))
        result = JadwalSholat(times)
        return result


    def getKajian(self):
        return self.getJson(f"{self.SERVER}/kajian/")

    def __init__(self, server):
        self.SERVER = server

