from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

islamicMonths = {
1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
}

class WelcomeFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("welcomeFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#welcomeFrame { background-color: black; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockWelcome = QtWidgets.QLabel(self)
        self.clockWelcome.setObjectName("clockWelcome")
        self.clockWelcome.setGeometry(50 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        self.clockWelcome.setStyleSheet(
           "#clockWelcome { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: "+config.textcolor2+"; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockWelcome.setAlignment(Qt.AlignLeft)

        self.todayDayWelcome = QtWidgets.QLabel(self)
        self.todayDayWelcome.setObjectName("todayDayWelcome")
        self.todayDayWelcome.setStyleSheet("#todayDayWelcome { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDayWelcome.setAlignment(Qt.AlignLeft)
        self.todayDayWelcome.setGeometry(50 * xscale, 125 * yscale, 500 * xscale, 100)

        self.todayDateWelcome = QtWidgets.QLabel(self)
        self.todayDateWelcome.setObjectName("todayDateWelcome")
        self.todayDateWelcome.setStyleSheet("#todayDateWelcome { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDateWelcome.setAlignment(Qt.AlignLeft)
        self.todayDateWelcome.setGeometry(50 * xscale, 175 * yscale, 500 * xscale, 100)

        self.hijriDateWelcome = QtWidgets.QLabel(self)
        self.hijriDateWelcome.setObjectName("hijriDateWelcome")
        self.hijriDateWelcome.setStyleSheet("#hijriDateWelcome { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.hijriDateWelcome.setAlignment(Qt.AlignLeft)
        self.hijriDateWelcome.setGeometry(50 * xscale, 225 * yscale, 500 * xscale, 100)

        self.insertPictTarhim = QtWidgets.QLabel(self)
        self.insertPictTarhim.setObjectName("insertPictTarhim")
        self.insertPictTarhim.setStyleSheet("#insertPictTarhim {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictTarhim.resize(width_label, height_label)
        self.insertPictTarhim.setPixmap(pixmap.scaled(self.insertPictTarhim.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictTarhim.setGeometry(635 * xscale, 775 * yscale, 150 * xscale, 100 * yscale)

        self.welcomeTo = QtWidgets.QLabel(self)
        self.welcomeTo.setObjectName("welcomeTo")
        self.welcomeTo.setWordWrap(True) 
        self.welcomeTo.setStyleSheet("#welcomeTo { font-family:Trebuchet MS; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(50 * xscale)) + "px;}")
        self.welcomeTo.setAlignment(Qt.AlignJustify)
        self.welcomeTo.setGeometry(175 * xscale, 450 * yscale, 400 * xscale, 100 * yscale)
        self.welcomeTo.setText(config.welcome)

        self.namaMasjid = QtWidgets.QLabel(self)
        self.namaMasjid.setObjectName("namaMasjid")
        self.namaMasjid.setWordWrap(True) 
        self.namaMasjid.setStyleSheet("#namaMasjid { font-family:Trebuchet MS; font-weight:light ;color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(40 * xscale)) + "px;}")
        self.namaMasjid.setAlignment(Qt.AlignJustify)
        self.namaMasjid.setGeometry(850 * xscale, 380 * yscale, 500 * xscale, 500)
        self.namaMasjid.setText(config.name + '\n' + config.address + '\n' + config.phone)

    def updateClock(self, timestr):
        self.clockWelcome.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDayWelcome.setText(day)
        self.todayDateWelcome.setText(ds)
        self.hijriDateWelcome.setText("{0} {1} {2}".format(islamicDate[2], islamicMonths[islamicDate[1]], islamicDate[0]))