from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

class ShalatFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("shalatFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#shalatFrame { background-color: blue; border-image: url(" +
                     config.shalatBackground+") 0 0 0 0 stretch stretch;}")
        self.clockShalatTime = QtWidgets.QLabel(self)
        self.clockShalatTime.setObjectName("clockShalatTime")
        self.clockShalatTime.setGeometry(550 * xscale, 350 * yscale, 350 * xscale, 100 * yscale)
        self.clockShalatTime.setStyleSheet(
           "#clockShalatTime { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: white; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockShalatTime.setAlignment(Qt.AlignLeft)
        self.clockShalatTime.setVisible(False)

        self.doPray = QtWidgets.QLabel(self)
        self.doPray.setObjectName("doPray")
        self.doPray.setStyleSheet("#doPray { font-family:Trebuchet MS; font-weight: bold; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.doPray.setAlignment(Qt.AlignLeft)
        self.doPray.setGeometry(575 * xscale, 400 * yscale, 1500 * xscale, 100)
        self.doPray.setText(config.doPray)

    def updateClock(self, timestr):
        self.clockShalatTime.setText(timestr.lower())
