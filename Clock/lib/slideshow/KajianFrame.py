from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

islamicMonths = {
1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
}

class KajianFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("kajianFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#kajianFrame { background-color: transparent; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockKajian = QtWidgets.QLabel(self)
        self.clockKajian.setObjectName("clockKajian")
        self.clockKajian.setGeometry(50 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        self.clockKajian.setStyleSheet(
           "#clockKajian { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: "+config.textcolor2+"; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockKajian.setAlignment(Qt.AlignLeft)

        self.todayDayKajian = QtWidgets.QLabel(self)
        self.todayDayKajian.setObjectName("todayDayKajian")
        self.todayDayKajian.setStyleSheet("#todayDayKajian { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDayKajian.setAlignment(Qt.AlignLeft)
        self.todayDayKajian.setGeometry(50 * xscale, 125 * yscale, 500 * xscale, 100)

        self.todayDateKajian = QtWidgets.QLabel(self)
        self.todayDateKajian.setObjectName("todayDateKajian")
        self.todayDateKajian.setStyleSheet("#todayDateKajian { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDateKajian.setAlignment(Qt.AlignLeft)
        self.todayDateKajian.setGeometry(50 * xscale, 175 * yscale, 500 * xscale, 100)

        self.hijriDateKajian = QtWidgets.QLabel(self)
        self.hijriDateKajian.setObjectName("hijriDateKajian")
        self.hijriDateKajian.setStyleSheet("#hijriDateKajian { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.hijriDateKajian.setAlignment(Qt.AlignLeft)
        self.hijriDateKajian.setGeometry(50 * xscale, 225 * yscale, 500 * xscale, 100)

        self.namaAcara = QtWidgets.QLabel(self)
        self.namaAcara.setObjectName("namaAcara")
        self.namaAcara.setWordWrap(True)
        self.namaAcara.setStyleSheet("#namaAcara { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(40 * xscale)) + "px;}")
        self.namaAcara.setAlignment(Qt.AlignLeft)
        self.namaAcara.setGeometry(100 * xscale, 450 * yscale, 500 * xscale, 1000 * yscale)

        self.temaAcara = QtWidgets.QLabel(self)
        self.temaAcara.setObjectName("temaAcara")
        self.temaAcara.setWordWrap(True)
        self.temaAcara.setStyleSheet("#temaAcara { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.temaAcara.setAlignment(Qt.AlignLeft)
        self.temaAcara.setGeometry(850 * xscale, 400 * yscale, 700 * xscale, 1000 * yscale)

        self.tempatAcara = QtWidgets.QLabel(self)
        self.tempatAcara.setObjectName("tempatAcara")
        self.tempatAcara.setWordWrap(True)
        self.tempatAcara.setStyleSheet("#tempatAcara { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.tempatAcara.setAlignment(Qt.AlignLeft)
        self.tempatAcara.setGeometry(850 * xscale, 450 * yscale, 700 * xscale, 1000 * yscale)

        self.tanggalAcara = QtWidgets.QLabel(self)
        self.tanggalAcara.setObjectName("tanggalAcara")
        self.tanggalAcara.setStyleSheet("#tanggalAcara { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.tanggalAcara.setAlignment(Qt.AlignLeft)
        self.tanggalAcara.setGeometry(850 * xscale, 500 * yscale, 700 * xscale, 1000 * yscale)

        self.waktuAcara = QtWidgets.QLabel(self)
        self.waktuAcara.setObjectName("waktuAcara")
        self.waktuAcara.setStyleSheet("#waktuAcara { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.waktuAcara.setAlignment(Qt.AlignLeft)
        self.waktuAcara.setGeometry(1010 * xscale, 500 * yscale, 700 * xscale, 1000 * yscale)

        self.keteranganAcara = QtWidgets.QLabel(self)
        self.keteranganAcara.setObjectName("keteranganAcara")
        self.keteranganAcara.setWordWrap(True)
        self.keteranganAcara.setStyleSheet("#keteranganAcara { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.keteranganAcara.setAlignment(Qt.AlignLeft)
        self.keteranganAcara.setGeometry(850 * xscale, 550 * yscale, 500 * xscale, 1000 * yscale)

        self.insertPictKajian = QtWidgets.QLabel(self)
        self.insertPictKajian.setObjectName("insertPictKajian")
        self.insertPictKajian.setStyleSheet("#insertPictKajian {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictKajian.resize(width_label, height_label)
        self.insertPictKajian.setPixmap(pixmap.scaled(self.insertPictKajian.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictKajian.setGeometry(635 * xscale, 775 * yscale, 150 * xscale, 100 * yscale)

    def updateClock(self, timestr):
        self.clockKajian.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDayKajian.setText(day)
        self.todayDateKajian.setText(ds)
        self.hijriDateKajian.setText("{0} {1} {2}".format(islamicDate[2], islamicMonths[islamicDate[1]], islamicDate[0]))

    def updateKajianDisplay(self,kajian):
        self.namaAcara.setText(kajian['fields']['nama_acara'])
        self.temaAcara.setText(kajian['fields']['tema'])
        self.tempatAcara.setText(kajian['fields']['tempat'])
        self.tanggalAcara.setText(kajian['fields']['tanggal_kadaluarsa']+",")
        self.waktuAcara.setText(kajian['fields']['waktu_kadaluarsa'])
        self.keteranganAcara.setText(kajian['fields']['keterangan'])

    def showDefaultKajianDisplay(self):
        self.namaAcara.setText('Tidak ada kajian')
        self.temaAcara.setText('')
        self.tempatAcara.setText('')
        self.tanggalAcara.setText('')
        self.waktuAcara.setText('')
        self.keteranganAcara.setText('')
