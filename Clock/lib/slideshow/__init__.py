
from lib.slideshow.MainSlideFrame import MainSlideFrame
from lib.slideshow.WelcomeFrame import WelcomeFrame
from lib.slideshow.KajianFrame import KajianFrame
from lib.slideshow.HaditsFrame import HaditsFrame
from lib.slideshow.AdzanFrame import AdzanFrame
from lib.slideshow.IqamahFrame import IqamahFrame
from lib.slideshow.ShalatFrame import ShalatFrame
from lib.slideshow.InfoFrame import InfoFrame
from lib.slideshow.SupportFrame import SupportFrame