from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

class AdzanFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("adzanFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#adzanFrame { background-color: blue; border-image: url(" +
                     config.adzanBackground+") 0 0 0 0 stretch stretch;}")
        self.clockAdzanTime = QtWidgets.QLabel(self)
        self.clockAdzanTime.setObjectName("clockAdzanTime")
        self.clockAdzanTime.setGeometry(550 * xscale, 385 * yscale, 350 * xscale, 100 * yscale)
        self.clockAdzanTime.setStyleSheet(
           "#clockAdzanTime { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: white; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockAdzanTime.setAlignment(Qt.AlignCenter)

        self.silentLabel = QtWidgets.QLabel(self)
        self.silentLabel.setObjectName("silentLabel")
        self.silentLabel.setStyleSheet("#silentLabel { font-family:Trebuchet MS; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.silentLabel.setAlignment(Qt.AlignLeft)
        self.silentLabel.setGeometry(150 * xscale, 325 * yscale, 1500 * xscale, 100)
        self.silentLabel.setText(config.TextAdzan)

        self.insertPictLogo = QtWidgets.QLabel(self)
        self.insertPictLogo.setObjectName("insertPictLogo")
        self.insertPictLogo.setStyleSheet("#insertPictLogo {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictLogo.resize(width_label, height_label)
        self.insertPictLogo.setPixmap(pixmap.scaled(self.insertPictLogo.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictLogo.setGeometry(650 * xscale, 775 * yscale, 150 * xscale, 100 * yscale)

    def updateClock(self, timestr):
        self.clockAdzanTime.setText(timestr.lower())
