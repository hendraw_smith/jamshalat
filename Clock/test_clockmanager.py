import unittest
import datetime
from lib.ClockManager import ClockManager
from lib.model.WaktuShalat import WaktuShalat
from lib.model.WaktuShalat import JadwalSholat


class TestClockManager(unittest.TestCase):

	def test_time_before_tarhim(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute = 11,52
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=0,microsecond=0)
		clock_manager = ClockManager()
		self.assertFalse(clock_manager.isTimeForTarhim(now, waktu_shalat))

	def test_time_for_tarhim(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute = 11,53
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=0,microsecond=0)
		clock_manager = ClockManager()
		self.assertTrue(clock_manager.isTimeForTarhim(now, waktu_shalat))

	def test_time_still_tarhim(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute, second = 11,53, 30
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		clock_manager = ClockManager()
		self.assertTrue(clock_manager.isTimeForTarhim(now, waktu_shalat))

	def test_time_not_anymore_tarhim(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute, second = 11,54,0
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		clock_manager = ClockManager()
		self.assertFalse(clock_manager.isTimeForTarhim(now, waktu_shalat))

	def test_time_before_adzan(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute = 11,53
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=0,microsecond=0)
		clock_manager = ClockManager()
		self.assertFalse(clock_manager.isTimeForAdzan(now, waktu_shalat))

	def test_time_for_adzan(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute = 11,54
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=0,microsecond=0)
		clock_manager = ClockManager()
		self.assertTrue(clock_manager.isTimeForAdzan(now, waktu_shalat))

	def test_time_10_seconds_after_adzan(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute, second = 11,54,10
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		clock_manager = ClockManager()
		self.assertTrue(clock_manager.isTimeForAdzan(now, waktu_shalat))
		
	def test_time_11_seconds_after_adzan(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute, second = 11,54,11
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		clock_manager = ClockManager()
		self.assertFalse(clock_manager.isTimeForAdzan(now, waktu_shalat))
		
	def test_time_iqamah_seconds(self):
		s = {"nama":"dhuhr","waktu":"11:54","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		waktu_shalat = WaktuShalat(s,s)
		hour, minute, second = 11,54,11
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		clock_manager = ClockManager()
		self.assertTrue(clock_manager.time_iqamah_seconds(now, waktu_shalat) > 0)


	def test_jadwal_sholat(self):
		l = [
		{"nama":"imsak","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"fajr","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"sunrise","waktu":"05:51","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"dhuhr","waktu":"12:00","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"asr","waktu":"15:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"sunset","waktu":"17:52","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"maghrib","waktu":"17:57","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"isha","waktu":"19:08","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"midnight","waktu":"23:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		]
		jadwal_shalat = JadwalSholat(l)
		self.assertNotEqual(0, len(jadwal_shalat.jadwal))
		self.assertEqual(list(jadwal_shalat.keys()),['fajr', 'sunrise', 'dhuhr', 'asr', 'maghrib', 'isha'] )
		l = list(jadwal_shalat.items())
		fajr = l[0][1]
		self.assertEqual('fajr',fajr.nama, msg=fajr.nama)
		self.assertEqual(datetime.time(4,43),fajr.waktu, msg=fajr.waktu)
		self.assertEqual(datetime.time(5,51),fajr.selesai, msg=fajr.selesai)
		hour, minute, second = 4, 50, 0
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertTrue(fajr.is_in(now))
		hour, minute, second = 6, 0 ,0
		now2 = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertFalse(fajr.is_in(now2))

	def test_jadwal_sholat_syuruq(self):
		l = [
		{"nama":"imsak","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"fajr","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"sunrise","waktu":"05:59","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"dhuhr","waktu":"12:00","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"asr","waktu":"15:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"sunset","waktu":"17:52","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"maghrib","waktu":"17:57","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"isha","waktu":"19:08","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"midnight","waktu":"23:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		]
		jadwal_shalat = JadwalSholat(l)
		self.assertEqual(list(jadwal_shalat.keys()),['fajr', 'sunrise', 'dhuhr', 'asr', 'maghrib', 'isha'] )
		l = list(jadwal_shalat.items())
		sunrise = l[1][1]
		hour, minute, second = 5, 59, 0
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertTrue(sunrise.is_in(now))
		hour, minute, second = 6, 0 ,0
		now2 = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertFalse(sunrise.is_in(now2))

	def test_jadwal_sholat_isya(self):
		l = [
		{"nama":"imsak","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"fajr","waktu":"04:43","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"sunrise","waktu":"05:59","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"dhuhr","waktu":"12:00","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"asr","waktu":"15:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"sunset","waktu":"17:52","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"maghrib","waktu":"17:57","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"isha","waktu":"19:08","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"},
		{"nama":"midnight","waktu":"23:21","durasi_adzan":"03:00","durasi_iqomah":"03:00","durasi_tarhim":"03:00","durasi_sholat":"03:00"}
		]
		jadwal_shalat = JadwalSholat(l)
		self.assertEqual(list(jadwal_shalat.keys()),['fajr', 'sunrise', 'dhuhr', 'asr', 'maghrib', 'isha'] )
		l = list(jadwal_shalat.items())
		isya = l[-1][1]
		hour, minute, second = 19, 8, 1
		now = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertTrue(isya.is_in(now), "{} {}".format(isya, now))
		hour, minute, second = 23, 59, 59
		midnight = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0) + datetime.timedelta(seconds=1)
		self.assertTrue(isya.is_isya(), "{} {}".format(isya, midnight))
		self.assertTrue(isya.is_before_end(midnight))
		self.assertTrue(isya.is_in(midnight), "{} {}".format(isya, midnight))
		hour, minute, second = 4, 43, 0
		timedout = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertTrue(isya.is_before_end(timedout))
		self.assertFalse(isya.is_in(timedout), "{} {}".format(isya, timedout))
		hour, minute, second = 4, 0, 0
		sahar = datetime.datetime.now().replace(hour=hour,minute=minute,second=second,microsecond=0)
		self.assertTrue(isya.is_before_end(sahar))
		self.assertFalse(isya.is_in(sahar), "{} {}".format(isya, sahar))



if __name__ == '__main__':
	unittest.main()