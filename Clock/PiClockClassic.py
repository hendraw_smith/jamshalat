# -*- coding: utf-8 -*-                 # NOQA

import sys
import os
import datetime
import time
import json
import locale
import webbrowser
import feedparser

from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtGui import QPainter, QImage, QFont
from PySide2.QtCore import QUrl
from PySide2.QtCore import Qt
from PySide2.QtNetwork import QNetworkReply
from PySide2.QtNetwork import QNetworkRequest
from subprocess import Popen

from lib.Configuration import Configuration
from lib.slideshow import MainSlideFrame, WelcomeFrame, KajianFrame, HaditsFrame, InfoFrame, SupportFrame, AdzanFrame, IqamahFrame, ShalatFrame
from lib.Repository import Repository
from lib.adzan import PlayFullAdzan, NotifyAdzan
from lib.ClockManager import ClockManager

sys.dont_write_bytecode = True
from praytimes import PrayTimes
from datetime import date

class MainWin(QtWidgets.QWidget):

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self)
        self.parent = parent

    def keyPressEvent(self, event):
        if isinstance(event, QtGui.QKeyEvent):
            if event.key() == Qt.Key_F4:
                self.parent.myquit()
            if event.key() == Qt.Key_F2:
                self.parent.lastkeytime = time.time() + 2
            if event.key() == Qt.Key_Space:
                self.parent.nextframe(1)
            if event.key() == Qt.Key_Left:
                self.parent.nextframe(-1)
            if event.key() == Qt.Key_Right:
                self.parent.nextframe(1)

            if event.key() == Qt.Key_A:
                self.parent.adzan()
            if event.key() == Qt.Key_I:
                self.parent.iqamah()
            if event.key() == Qt.Key_S:
                self.parent.shalat()

    def mousePressEvent(self, event):
        if type(event) == QtGui.QMouseEvent:
            self.parent.nextframe(1)


class SlideshowClock:
    ADZAN_TO_IQAMAH_DURATION = 10 * 60
    IQAMAH_TO_SHALAT_DURATION = 15
    SHALAT_DURATION = 10 * 60
    SLIDESHOW_DURATION = 10
    UPDATE_DATA_EVERY = 60
    ADZAN_DURATION = 5 * 60

    SLIDESHOW_DURATION = 3
    ADZAN_DURATION = 10
    ADZAN_TO_IQAMAH_DURATION = 15
    IQAMAH_TO_SHALAT_DURATION = 15

    def changeFrame(self, frame):
        self.currentFrame.setVisible(False)
        if frame != self.iqamahFrame : 
            frame.updateClock(self.timeformat(datetime.datetime.now()))
        self.currentFrame=frame
        self.currentFrame.setVisible(True)

    def adzan(self):
        self.state="ADZAN"
        self.changeFrame(self.adzanFrame)
        adzanTimes = ["SHUBUH","SYURUQ","ZHUHUR","ASHAR", "MAGHRIB", "ISYA"]
        adzanTime = adzanTimes[self.alarm_index]
        if(adzanTime != "SYURUQ"):
            self.adzanPlayer.play(adzanTime)

    def iqamah(self):
        self.state="IQAMAH"
        self.changeFrame(self.iqamahFrame)
    def shalat(self):
        self.state="SHALAT"
        self.changeFrame(self.shalatFrame)
    def showNormal(self):
        self.state="NORMAL"
        self.changeFrame(self.mainSlideFrame)

    def updateClock(self, now):
        if self.state=="NORMAL":
            timestr = self.timeformat(now)
            self.currentFrame.updateClock(timestr)

    def timeformat(self, now):
        timestr = self.config.digitalformat.format(now)
        if self.config.digitalformat.find("%I") > -1:
            if timestr[0] == '0':
                timestr = timestr[1:99]
        return timestr

    def updateData(self):
        self.updateKajian()
        self.updateHadits()

    def updateKajian(self):
        self.kajianList =  self.repository.getKajian()
        self.kajianLength = len(self.kajianList)

    def updateHadits(self):
        self.haditsList = self.repository.getHadits()
        self.haditsLength = len(self.haditsList)

    def updateInfo(self):
        self.infoList = self.repository.getInfo()
        self.infoLength = len(self.infoList)

    def nextSlide(self):
        if self.state!="NORMAL":
            return

        if(self.currentFrame == self.kajianFrame):
            if(len(self.kajianList)>0):
                self.kajianIndex = (self.kajianIndex+1) % len(self.kajianList)
            if(self.kajianIndex > 0):
                self.kajianFrame.updateKajianDisplay(self.kajianList[self.kajianIndex])
            else:
                self.slideIndex = (self.slideIndex+1) % len(self.slideFrames)
                nextframe = self.slideFrames[self.slideIndex]
                self.changeFrame(nextframe)
        elif(self.currentFrame == self.infoFrame):
            if(len(self.infoList)>0):
                self.infoIndex = (self.infoIndex+1) % len(self.infoList)
            if(self.infoIndex > 0):
                self.infoFrame.updateInfoDisplay(self.infoList[self.infoIndex])
            else:
                self.slideIndex = (self.slideIndex+1) % len(self.slideFrames)
                nextframe = self.slideFrames[self.slideIndex]
                self.changeFrame(nextframe)

        else:
            self.slideIndex = (self.slideIndex+1) % len(self.slideFrames)
            nextframe = self.slideFrames[self.slideIndex]
            if(nextframe == self.kajianFrame):
                self.updateKajian()
                if(len(self.kajianList)==0):
                    self.kajianFrame.showDefaultKajianDisplay()
                else:
                    self.kajianIndex = 0
                    self.kajianFrame.updateKajianDisplay(self.kajianList[self.kajianIndex])
            if(nextframe == self.haditsFrame):
                self.updateHadits()
            if(nextframe == self.infoFrame):
                self.updateInfo()
                if(len(self.infoList)==0):
                    self.infoFrame.showDefaultInfoDisplay()
                else:
                    self.infoIndex = 0
                    self.infoFrame.updateInfoDisplay(self.infoList[self.infoIndex])
            self.changeFrame(nextframe)

        if self.currentFrame == self.haditsFrame:
            self.haditsFrame.updateHaditsDisplay(self.haditsList[self.haditsIndex])
            self.haditsIndex = (self.haditsIndex+1) % len(self.haditsList)

    def tick(self):
        now = datetime.datetime.now()
        self.doTick(now)

    def doTick(self, now):
        if self.config.DateLocale != "":
            try:
                locale.setlocale(locale.LC_TIME, 'IND')
            except:
                pass

        timestr = self.timeformat(now)
        if self.lasttimestr != timestr:
            self.updateClock(now)
        self.handleState()
        self.lasttimestr = timestr

        # Set format tanggal indonesia, hendra    
        if now.day != self.lastday:
            self.lastday = now.day
            self.updateDateDisplay(now)

    def updateDateDisplay(self, now):
        # date
        day = "{0:%A}".format(now)
        ds = "{0.day} {0:%B} {0.year}".format(now)
        from convertdate import islamic

        islamicDate = islamic.from_gregorian(now.year, now.month, now.day)
        # print(islamicDate)
        self.mainSlideFrame.updateDate(day, ds, islamicDate)
        self.welcomeFrame.updateDate(day, ds, islamicDate)
        self.kajianFrame.updateDate(day, ds, islamicDate)
        self.haditsFrame.updateDate(day, ds, islamicDate)
        self.infoFrame.updateDate(day, ds, islamicDate)
        self.supportFrame.updateDate(day, ds, islamicDate)

        self.data_adzan = self.updateDataAdzan()
        self.mainSlideFrame.updateSholatDisplay([i.waktu.strftime("%H:%M") for i in self.data_adzan])

    def updateDataAdzan(self):
        times = self.repository.getJadwalSholat()
        return times.as_list()

    def handleState(self):
        now = datetime.datetime.now()
        self.alarm_index = self.getCurrentTimeIndex(now)
        pray_time = self.data_adzan[self.alarm_index]
        
        if self.state=="NORMAL" and self.clockManager.isTimeForAdzan(now, pray_time):
            self.adzan()
        # print("state1 = %s"%self.state)

        if self.state=="ADZAN" and self.clockManager.isTimeToShowNormalBeforeIqamah(now, pray_time):
            self.updatePrayTime(now)
            self.showNormal()
        # print("state2 = %s"%self.state)

        if self.clockManager.isTimeForIqamah(now, pray_time):
            self.iqamah()
        # print("state3 = %s"%self.state)

        if self.state=="IQAMAH":
            deltastr = str(datetime.timedelta(seconds=self.clockManager.time_iqamah_seconds(now, pray_time)))
            self.iqamahFrame.setCountdown(deltastr)
            if self.clockManager.isTimeForShalat(now, pray_time):
                self.shalat()
        # print("state4 = %s"%self.state)

        if self.state=="SHALAT":
            if self.clockManager.isShalatDone(now, pray_time):
                self.showNormal()

    def qtstart(self):
        global ctimer
        global slideshowTimer
        global datas
        datas = self.repository.getKajian()
        ctimer = QtCore.QTimer()
        ctimer.timeout.connect(self.tick)
        ctimer.start(1000)
        slideshowTimer = QtCore.QTimer()
        slideshowTimer.timeout.connect(self.nextSlide)
        slideshowTimer.start(self.SLIDESHOW_DURATION*1000)

    def realquit():
        QtWidgets.QApplication.exit(0)


    def myquit(a=0, b=0):
        global ctimer
        global jadwalTimer
        global slideshowTimer
        ctimer.stop()
        slideshowTimer.stop()
        QtCore.QTimer.singleShot(30, realquit)


    def nextframe(self,plusminus):
        self.frames[self.framep].setVisible(False)
        self.framep += plusminus
        if self.framep >= len(self.frames):
            self.framep = 0
        if self.framep < 0:
            self.framep = len(self.frames) - 1
        self.frames[self.framep].setVisible(True)

    def __init__(self, config, repository, adzanPlayer, clockManager):
        self.repository = repository
        self.clockManager = clockManager
        webConfig = repository.getSetting()[0]["fields"]
        config.name = webConfig["nama"]
        config.address = webConfig["alamat"]
        config.phone = webConfig["telepon"]
        if webConfig["suara"]=="false":
            adzanPlayer.mute()
        else:
            adzanPlayer.unmute()
        adzanPlayer.setVolume(int(webConfig["volume"]))
        self.config = config
        self.adzanPlayer = adzanPlayer

        self.lastday = -1
        self.lasttimestr = ""
        self.lastkeytime = 0
        if self.config.DateLocale != "":
            try:
                locale.setlocale(locale.LC_TIME, 'IND')
            except:
                pass

        self.state = 'NORMAL'

        app = QtWidgets.QApplication(sys.argv)
        desktop = app.desktop()
        rec = desktop.screenGeometry()
        height = rec.height()
        width = rec.width()

        w = MainWin(self)

        w.setStyleSheet("QWidget { background-color: black;}")

        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0

        self.frames = []
        self.framep = 0
        self.kajianList = []
        self.haditsList = []
        self.infoList = []
        self.kajianLength = 0
        self.haditsLength = 0
        self.infoLength = 0
        self.slideFrames = []


        self.initFrames(w, width, height)
        self.updateData()
                
        self.data_adzan = self.updateDataAdzan()

        self.seconds_hms = [3600, 60, 1] # Number of seconds in an Hour, Minute, and Second

        nowX = datetime.datetime.now()

        self.adzanplayer = None

        self.updatePrayTime(nowX)
        self.kajianIndex = 0
        self.haditsIndex = 0
        self.infoIndex = 0

        manager = QtNetwork.QNetworkAccessManager()
        self.mainWin = w
        self.app = app

    def updatePrayTime(self, now):
        self.alarm_index = self.getCurrentTimeIndex(now)
        self.mainSlideFrame.time_active(self.alarm_index)

    def getCurrentTimeIndex(self, now):
        try:
            is_ins = [a.is_in(now) for a in self.data_adzan]
            a_idx = is_ins.index(True)
            return a_idx
        except:
            return -1

    def initFrames(self, mainWin, width, height):
       #Frame utama saat pertama kali tampil
        print("make self.mainSlideFrame")

        self.mainSlideFrame = MainSlideFrame(mainWin, self.config, width, height)
        #self.mainSlideFrame.setVisible(False)
        self.frames.append(self.mainSlideFrame)
        print("made self.mainSlideFrame")

        #frame info masjid
        self.welcomeFrame = WelcomeFrame(mainWin, self.config, width, height)
        self.welcomeFrame.setVisible(False)
        self.frames.append(self.welcomeFrame)
        print("made self.welcomeFrame")

        #frame countdown waktu tarhim
        self.kajianFrame = KajianFrame(mainWin, self.config, width, height)
        self.kajianFrame.setVisible(False)
        self.frames.append(self.kajianFrame)
        print("made self.kajianFrame")

        self.haditsFrame = HaditsFrame(mainWin, self.config, width, height)
        self.haditsFrame.setVisible(False)
        self.frames.append(self.haditsFrame)
        print("made self.haditsFrame")

        #frame info atau pengumuman masjid
        self.infoFrame = InfoFrame(mainWin, self.config, width, height)
        self.infoFrame.setVisible(False)
        self.frames.append(self.infoFrame)
        print("made self.infoFrame")

        print("make self.supportFrame")
        self.supportFrame = SupportFrame(mainWin, self.config, width, height)
        self.supportFrame.setVisible(False)
        self.frames.append(self.supportFrame)
        print("made self.supportFrame")

        #frame saat sedang self.adzan
        print("make self.adzanFrame")
        self.adzanFrame = AdzanFrame(mainWin, self.config, width, height)
        self.adzanFrame.setVisible(False)
        self.frames.append(self.adzanFrame)
        print("made self.adzanFrame")

        self.iqamahFrame = IqamahFrame(mainWin, self.config, width, height)
        self.iqamahFrame.setVisible(False)
        self.frames.append(self.iqamahFrame)
        print("made self.iqamahFrame")

        #frame saat sedang melaksanakan shalat
        print("make self.shalatFrame")
        self.shalatFrame = ShalatFrame(mainWin, self.config, width, height)
        self.shalatFrame.setVisible(False)
        self.frames.append(self.shalatFrame)
        print("made self.shalatFrame")

        self.currentFrame = self.mainSlideFrame

        self.slideFrames = [self.mainSlideFrame, self.welcomeFrame, self.kajianFrame, self.haditsFrame, self.infoFrame, self.supportFrame]
        self.slideIndex = 0

    def run(self):
        stimer = QtCore.QTimer()
        stimer.singleShot(10, self.qtstart)

        self.mainWin.show()
        self.mainWin.showFullScreen()

        sys.exit(self.app.exec_())


if __name__ == '__main__':
    SERVER="http://localhost:8000"
    config = Configuration.fetch()
    repository = Repository(SERVER)
    # self.adzanPlayer = PlayFullAdzan(self.config)
    adzanPlayer = NotifyAdzan(config)

    clockManager = ClockManager()

    a=SlideshowClock(config, repository, adzanPlayer, clockManager)
    a.run()
